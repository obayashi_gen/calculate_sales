package jp.alhinc.obayashi_gen.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class CalclateSales implements Serializable {
	public static boolean inputBranchOffice(int argsLen, String[] args, HashMap<String,Long> totalmap, HashMap<String,String> shopmap) {
		BufferedReader br = null;
		try {
			if (!(argsLen == 1)) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}

			File file = new File(args[0], "branch.lst");

			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) != null) {
				String[] shopes = line.split(",");
				if (shopes[0].matches("\\d{3}") && shopes.length == 2) {
					shopmap.put(shopes[0], shopes[1]);
					totalmap.put(shopes[0], (long) 0);
				}
			    else {
			    	System.out.println("支店定義ファイルのフォーマットが不正です");
			        return false;
			    }
			}
		}
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(br != null) {
				try {
					br.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	public static boolean CheckSerialNumber(ArrayList<File> CheckEndList, ArrayList<Integer> numberCheckList) {
		for(int i=0; i<CheckEndList.size(); i++) {
			if (!(numberCheckList.get(i).equals(i + 1))) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}
		return true;
	}


	public static boolean aggregateSalesTotal(ArrayList<File> CheckEndList, File[] files, HashMap<String,Long> totalmap) {
		BufferedReader br2 = null;
		try {
			for(int i=0; i<CheckEndList.size(); ++i) {
				br2 = new BufferedReader(new FileReader(CheckEndList.get(i)));
				String s;
				ArrayList<String> proceeds = new ArrayList<String>();

				while((s = br2.readLine()) != null) {
					proceeds.add(s);
				}

				if (!(proceeds.size() == 2)) {
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return false;
				}

				if (!(proceeds.get(1).matches("^[0-9]*$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				if (!(totalmap.containsKey(proceeds.get(0)))) {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return false;
				}

				Long lon = totalmap.get(proceeds.get(0)) + Long.parseLong(proceeds.get(1));

				if (lon >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}

				totalmap.put(proceeds.get(0), lon);
			}
		}
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(br2 != null) {
				try {
					br2.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	public static boolean outputToFile(String[] args, HashMap<String,Long> totalmap, HashMap<String,String> shopmap) {
		BufferedWriter bw = null;
		try {
			File file = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String mapkey : totalmap.keySet()) {
				bw.write(mapkey + "," + shopmap.get(mapkey) + "," + totalmap.get(mapkey));
				bw.newLine();
			}
		}
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(bw != null) {
				try {
					bw.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		HashMap<String,String> shopmap = new HashMap<String,String>();
		HashMap<String,Long> totalmap = new HashMap<String,Long>();
		int argsLen = args.length;

		if (!(inputBranchOffice(argsLen, args, totalmap, shopmap))) {
			return;
		}

		File dir = new File(args[0]);
		File[] files = dir.listFiles();
		ArrayList<Integer> numberCheckList  = new ArrayList<Integer>();
		ArrayList<File> CheckEndList  = new ArrayList<File>();

		for(int i=0; i<files.length; ++i) {
			if (files[i].getName().matches("^\\d{8}.rcd") && files[i].isFile()){
				String baseName = files[i].getName();
				String fileName = baseName.substring(0,baseName.lastIndexOf('.'));
				int intFileName = Integer.parseInt(fileName);
				numberCheckList.add(intFileName);
				Collections.sort(numberCheckList);

				File file2 = new File(files[i].toString());
				CheckEndList.add(file2);
			}
		}

		if(!CheckSerialNumber(CheckEndList, numberCheckList)) {
			return;
		}

		if(!aggregateSalesTotal(CheckEndList, files, totalmap)) {
			return;
		}

		if (!(outputToFile(args, totalmap, shopmap))) {
			return;
		}
	}
}
